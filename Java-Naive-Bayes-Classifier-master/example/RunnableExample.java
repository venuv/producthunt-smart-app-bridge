//package de.daslaboratorium.machinelearning.bayes.example;
package Examples;

import java.util.Arrays;

import naivebayes.BayesClassifier;
import naivebayes.Classifier;

public class RunnableExample {

    public static void main(String[] args) {

        /*
         * Create a new classifier instance. The context features are
         * Strings and the context will be classified with a String according
         * to the featureset of the context.
         */
        final Classifier<String, String> bayes =
                new BayesClassifier<String, String>();

        /*
         * The classifier can learn from classifications that are handed over
         * to the learn methods. Imagin a tokenized text as follows. The tokens
         * are the text's features. The category of the text will either be
         * positive or negative.
         */
	//        final String[] positiveText = "I love sunny days".split("\\s");
	//        bayes.learn("positive", Arrays.asList(positiveText));
	String [] sportText, travelText, foodText,unknownText;
	sportText = "Sports Injury Predictor Predicts Injuries for NFL Players".split("\\s");
	bayes.learn("Sports", Arrays.asList(sportText));
	sportText = "Reuters Sports Reel Near-live photo coverage from major sporting events".split("\\s");
	bayes.learn("Sports", Arrays.asList(sportText));
	sportText = "Hides NFL, NBA, MLB and MLS teams hashtags from your Twitter".split("\\s");
	bayes.learn("Sports", Arrays.asList(sportText));
	sportText = "Daily Fantasy Sports for Real Money".split("\\s");
	bayes.learn("Sports", Arrays.asList(sportText));
	sportText = "Sports Stories Told by the Players, Coaches, and Insiders".split("\\s");
	bayes.learn("Sports", Arrays.asList(sportText));
	sportText = "Live sports news network for the mobile generation".split("\\s");
	bayes.learn("Sports", Arrays.asList(sportText));
	sportText = "Local marketplace for athletes to buy and sell sports gear".split("\\s");
	bayes.learn("Sports", Arrays.asList(sportText));

	travelText = "Collects travel videos taken by drones".split("\\s");
	bayes.learn("Travels", Arrays.asList(travelText));
	travelText = "Knows what to pack, where to eat and what to see".split("\\s");
	bayes.learn("Travels", Arrays.asList(travelText));
	travelText = "Modern guides to travel".split("\\s");
	bayes.learn("Travels", Arrays.asList(travelText));
	travelText = "Travel Plans by Locals".split("\\s");
	bayes.learn("Travels", Arrays.asList(travelText));
	travelText = "Free one-way rental cars for flexible travellers".split("\\s");
	bayes.learn("Travels", Arrays.asList(travelText));
	travelText = "Travel off the Eaten Path".split("\\s");
	bayes.learn("Travels", Arrays.asList(travelText));
	travelText = "Villy is Your 21st Century Travel Agent".split("\\s");
	bayes.learn("Travels", Arrays.asList(travelText));



	foodText = "Discover, organize and share the best dishes in the world".split("\\s");
	bayes.learn("Food", Arrays.asList(foodText));
	foodText = "Product Hunt for natural foods & products".split("\\s");
	bayes.learn("Food", Arrays.asList(foodText));
	foodText = "Secret menu items available at fast-food restaurants".split("\\s");
	bayes.learn("Food", Arrays.asList(foodText));
	foodText = "Healthy food takeout and delivery app".split("\\s");
	bayes.learn("Food", Arrays.asList(foodText));
	foodText = "Grow the freshest food on Earth right from your smartphone".split("\\s");
	bayes.learn("Food", Arrays.asList(foodText));
	foodText = "Relay Foods Healthy shopping has never been this simple".split("\\s");
	bayes.learn("Food", Arrays.asList(foodText));
	foodText = "Secret menu items available at fast-food restaurants".split("\\s");
	bayes.learn("Food", Arrays.asList(foodText));

	unknownText = "Some filler junk here".split("\\s");
	bayes.learn("Food", Arrays.asList(unknownText));

	
//        final String[] negativeText = "I hate rain".split("\\s");
	//        bayes.learn("negative", Arrays.asList(negativeText));

        /*
         * Now that the classifier has "learned" two classifications, it will
         * be able to classify similar sentences. The classify method returns
         * a Classification Object, that contains the given featureset,
         * classification probability and resulting category.
         */
        final String[] unknownText1 = "Browse thousands of route-by-route travel plans".split("\\s");
        final String[] unknownText2 = "Private air-travel membership".split("\\s");
        final String[] unknownText3 = "Live sports news network for the mobile generation".split("\\s");
        final String[] unknownText4 = "Browse thousands of route-by-route travel plans".split("\\s");
        final String[] unknownText5 = "Five bite-sized food documentaries served weekly".split("\\s");
        final String[] unknownText6 = "Tumblr for cooks, an alternative to food blogging".split("\\s");
        final String[] unknownText7 = "World's first auto-follow action sports drone".split("\\s");
        final String[] unknownText8 = "Mobile Sports Trivia".split("\\s");

	System.out.println("*********");
	try {
	    System.out.println( unknownText1+" is "+
                bayes.classify(Arrays.asList(unknownText1)).toString());
	} catch (Exception e) {
	    System.out.println(unknownText1+ " Class unknown");
	};

	System.out.println("*********");
	try {
	    System.out.println( unknownText2+" is "+ 
                bayes.classify(Arrays.asList(unknownText2)).toString());
	}
	catch (Exception e) {System.out.println("Private air-travel membership"+"Class unknown");};

	System.out.println("*********");
	try {
	    System.out.println( unknownText3+" is "+ 
                bayes.classify(Arrays.asList(unknownText3)).toString());
	}
	catch (Exception e) {System.out.println("Live sports news network for the mobile generation"+" Class unknown");};

	System.out.println("*********");
	try {
        System.out.println( unknownText4+" is "+
                bayes.classify(Arrays.asList(unknownText4)).toString());
	}
	catch (Exception e) {System.out.println("Class unknown");};

	System.out.println("*********");
	try {
	    System.out.println( unknownText5+" is "+
                bayes.classify(Arrays.asList(unknownText5)).toString());
	}
	catch (Exception e) {System.out.println("Class unknown");};

	System.out.println("*********");
	try {
	    System.out.println( unknownText6+" is "+
                bayes.classify(Arrays.asList(unknownText6)).toString());
	}
	catch (Exception e) {System.out.println(unknownText6+" Class unknown");};

	System.out.println("*********");
	try {
        System.out.println( unknownText7+" is "+
                bayes.classify(Arrays.asList(unknownText7)).toString());
	}
	catch (Exception e) {System.out.println("Class unknown");};

	System.out.println("*********");
	try {
        System.out.println( unknownText8+" is "+
                bayes.classify(Arrays.asList(unknownText8)).toString());
	}
	catch (Exception e) {System.out.println("Class unknown");};

        /*
         * The BayesClassifier extends the abstract Classifier and provides
         * detailed classification results that can be retrieved by calling
         * the classifyDetailed Method.
         *
         * The classification with the highest probability is the resulting
         * classification. The returned List will look like this.
         * [
         *   Classification [
         *     category=negative,
         *     probability=0.0078125,
         *     featureset=[today, is, a, sunny, day]
         *   ],
         *   Classification [
         *     category=positive,
         *     probability=0.0234375,
         *     featureset=[today, is, a, sunny, day]
         *   ]
         * ]
         */
	//        ((BayesClassifier<String, String>) bayes).classifyDetailed(
	//                Arrays.asList(unknownText1));

        /*
         * Please note, that this particular classifier implementation will
         * "forget" learned classifications after a few learning sessions. The
         * number of learning sessions it will record can be set as follows:
         */
        bayes.setMemoryCapacity(500); // remember the last 500 learned classifications
    }

}
