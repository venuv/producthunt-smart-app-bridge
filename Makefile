tvhunt3: app_pop3
	java -classpath "./libs/httpclient-4.3.5.jar:./libs/httpcore-4.3.2.jar::./libs/commons-logging-1.1.3.jar:./libs/json-20140107.jar:./libs/nbc.jar:./libs/lingpipe-4.1.0.jar" app_populator_scoringfn 14 Sport

app_pop3: app_populator_scoringfn.java
	javac -classpath "./libs/httpclient-4.3.5.jar:./libs/httpcore-4.3.2.jar:./libs/nbc.jar:./libs/json-20140107.jar:./libs/lingpipe-4.1.0.jar" app_populator_scoringfn.java -Xlint


nbc:
	cd Java-Naive-Bayes-Classifier-master; \
	javac Classification.java IFeatureProbability.java Classifier.java BayesClassifier.java; \
	mv *.class naivebayes; \
	jar cvf nbc.jar naivebayes/*.class; \
	cp nbc.jar ../libs/; \
	cd ..