import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.*;
import java.util.Arrays;

import naivebayes.BayesClassifier;
import naivebayes.Classifier;

import com.aliasi.tokenizer.Tokenizer;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.TokenizerFactory;
import com.aliasi.tokenizer.EnglishStopTokenizerFactory;
import com.aliasi.tokenizer.LowerCaseTokenizerFactory;

import org.apache.http.client.*;
import org.apache.http.impl.client.*;
import org.apache.http.client.methods.*;
import org.json.JSONObject;
import org.json.JSONArray;
import java.lang.ProcessBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.HttpResponse;
import java.net.URLEncoder;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class app_populator_scoringfn {

    static class Category implements Comparable<Category> {
	String name;
	int score;

	public Category(String nm,int sc) {
	    name = nm;
	    score = sc;
	}


	public int compareTo(Category cg) {
 
	    int compareScore = cg.score;
 
	    return this.score - compareScore;
	}
    }

    static HashMap <String,Integer> sport_map, food_map, travel_map,finance_map;
    static Set<Integer>item_id_set = new HashSet<Integer>();

    static void initClassification () {
	sport_map = new HashMap <String,Integer>();
	food_map = new HashMap <String,Integer>();
	travel_map = new HashMap <String,Integer>();
	finance_map = new HashMap <String,Integer>();

	finance_map.put ("money",10);
	finance_map.put ("chari",5);
	finance_map.put ("stock",15);
	finance_map.put ("tradi",10);
	finance_map.put ("trade",10);
	finance_map.put ("marke",10);
	finance_map.put ("tick",10);
	finance_map.put ("tips",10);
	finance_map.put ("pick",5);
	finance_map.put ("inves",5);
	finance_map.put ("chart",10);

	sport_map.put ("sport",20);
	sport_map.put ("injur",10);
	sport_map.put ("team",10);
	sport_map.put ("triv",5);
	sport_map.put ("fanta",5);

	travel_map.put("trave",20);
	travel_map.put("plan",10);
	travel_map.put("see",10);
	travel_map.put("guide",5);
	travel_map.put("touri",5);

	food_map.put("food",20);
	food_map.put("foodi",10);
	food_map.put("chef",5);
	food_map.put("eat",10);
	food_map.put("cook",5);
    }

    static String[] lingClean(String text) {
	TokenizerFactory f1 = IndoEuropeanTokenizerFactory.INSTANCE;
	TokenizerFactory f2 = new LowerCaseTokenizerFactory(f1);
	TokenizerFactory f3 = new EnglishStopTokenizerFactory(f2);

	text = text.replace('-',' ');        
	char[] cs =text.toCharArray();
	Tokenizer tokenizer = f3.tokenizer(cs, 0, cs.length);
	String[] tokens = tokenizer.tokenize();
	for (int i=0;i<tokens.length;i++) {
	    tokens[i] = tokens[i].substring(0, Math.min(tokens[i].length(), 5));
	}

	//	System.out.println("[tvhunt3]: Token 0 = "+tokens[0]);
	return tokens;
    }


    static String computeKind(String[] tokens) {
	int food_score=0;
	int travel_score=0;
	int sport_score = 0;
	int fin_score = 0;

	for (int i=0;i<tokens.length;i++) {
	    
	    if (sport_map.get(tokens[i])!=null)
		sport_score += sport_map.get(tokens[i]);
	    if (travel_map.get(tokens[i])!=null)
		travel_score += travel_map.get(tokens[i]);
	    if (food_map.get(tokens[i])!=null)
		food_score += food_map.get(tokens[i]);
	    if (finance_map.get(tokens[i])!=null)
		fin_score += finance_map.get(tokens[i]);
	}

	Category[] cats = new Category[4];
	cats[0] = new Category("Sport",sport_score);
	cats[1] = new Category("Travel",travel_score);
	cats[2] = new Category("Food",food_score);
	cats[3] = new Category("Finance",fin_score);
	Arrays.sort(cats);



	if (cats[3].score<20) return("Unknown");

	//	System.out.println("Scoring - "+cats[0].name+" "+cats[0].score+" "
	//			   +cats[1].name+" "+cats[1].score+" "
	//			   +cats[2].name+" "+cats[2].score);

	return(cats[3].name);
    }


    public static String getPlatform( JSONObject item) {
	String name = item.getString("name");
	String tagline = item.getString("tagline");

	String norm_name = name.toLowerCase();
	String norm_tagline = tagline.toLowerCase();


	if (norm_name.contains("ios")||
	    norm_name.contains("iOS")||
	    norm_tagline.contains("ios")||
	    norm_tagline.contains("iOS")) {
	    return("iOS");
	}

	if (norm_name.contains("android")||
	    norm_name.contains("Android")||
	    norm_tagline.contains("android")||
	    norm_tagline.contains("Android")) {
	    return("Android");
	}

	return("Web");
    }

    public static boolean newItem(int item_id) {
	if (item_id_set.contains((Integer)item_id)) return false;

	item_id_set.add((Integer)item_id);
	return true;
    }


    public static void main(String[] args) {

	CloseableHttpClient client = HttpClients.createDefault();
	HttpGet request;

	CloseableHttpResponse response;
	StringBuffer sb = new StringBuffer();

	int start_day = Integer.parseInt(args[0]);
	String seed = args[1];

	initClassification();

	for (int days=start_day;days<start_day+2;days++) {
	    System.out.println(Integer.toString(days)+"***Days Ago ***");
	    //	    request = new HttpGet("https://api.producthunt.com/v1/posts?access_token=eeb16a1eb423b3c97cc501f7c0d7a69d79bfe096e3aab31bfe078aed8ffcb357&days_ago="+Integer.toString(days));
	    request = new HttpGet("https://api.producthunt.com/v1/posts?access_token=<access_token>&days_ago="+Integer.toString(days));
	    try {
		//		Thread.sleep(2000);
		response = client.execute(request);
		BufferedReader rd = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
		    sb.append(line);
		}
	    }
	    catch (Exception e) {
		//	    e.printStackTrace();
	    }
	    finally {
	    }

	    String file_as_string = sb.toString();
	    System.out.println("output = "+file_as_string);
	    JSONObject output = new JSONObject(file_as_string);
	    System.out.println("output length = "+output.length());
	    JSONArray arr = output.getJSONArray("posts");

	    for (int i = 0; i < arr.length(); i++)
		{
		    JSONObject item = arr.getJSONObject(i);
		    int post_id = item.getInt("id");
		    String tagline = item.getString("tagline");


		    String platform_type = getPlatform(item);
		    String app_kind = computeKind(lingClean(tagline));

		    String redir_url = item.getString("redirect_url");
		    String title = item.getString("name");

		    if ((app_kind.contains("port")||app_kind.contains("ravel")||app_kind.contains("ood")||app_kind.contains("inanc")) && newItem(post_id)) {

			System.out.println("**** Tagline - "+tagline);

			String TVAPP_URL = "https://api.parse.com/1/classes/tvapp";
			HttpClient httpclient = HttpClientBuilder.create().build();
			HttpPost httppost = new HttpPost(TVAPP_URL);
			JSONObject appJson = new JSONObject(); 
			appJson.put("Id",post_id);
			appJson.put("Kind",app_kind);
			appJson.put("Platform",platform_type);
			appJson.put("Tagline",tagline);
			appJson.put("Title",title);
			appJson.put("EntryType","Script");
			appJson.put("Url",redir_url);
			httppost.addHeader("X-Parse-Application-Id", <Application-Id>);
			httppost.addHeader("X-Parse-REST-API-Key", <REST-API-Key>);
			httppost.addHeader("Content-Type", "application/json");
			try {

			    httppost.setEntity(new StringEntity(appJson.toString()));
			    HttpResponse httpresponse = httpclient.execute(httppost);

			    System.out.println("Posting object - "+ appJson.toString());
			    System.out.println("App Entry Post Status : "+httpresponse.getStatusLine());
			} catch (Exception e) {
			    e.printStackTrace();
			}




		    }


		}
	}

    }
}
